const COMMON_CONFIG = {
    // 消息类型
    notificationType: {
        10: "审批通知",
        11: "审批抄送"
    },
}

export default COMMON_CONFIG